<?php
/*
Template Name: Events
*/

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

$post = Timber::get_post();

$context['post'] = $post;

$post_slug = $post->slug;

$context['post_slug'] = $post_slug;

$context['post_title'] = $post->title;


$url = $_SERVER['REQUEST_URI'];
$substr_url = substr($url, strrpos($url, 's-') + 2);
$last_word = rtrim($substr_url, '/');

$context['last_word'] = $last_word;

$eventterm = array(); 

$terms = get_terms( array(
	'taxonomy' => 'beach_access_type',
	'hide_empty' => false,
) );

// $context['terms'] = $terms;

// foreach ($terms as $term ){
// 	$eventterm[] = $term->slug;
// }

$page_slug = $post->slug;
$context['post_slug'] = $page_slug;

if(isset($_GET['season'])){
	$query_year = array(
		'taxonomy' => 'beach_access_type',
		'field' => 'slug',
		'terms' => array($_GET['season']),
		'operator' => 'IN',
		'include_children' => false // Remove if you need posts from term 7 child terms
	);
} elseif(isset($_GET['discipline'])){
	$query_year = array(
		'taxonomy' => 'beach_access_type',
		'field' => 'slug',
		'terms' => array("2018","2019","2020","2021","2022"),
		'operator' => 'IN',
		'include_children' => false // Remove if you need posts from term 7 child terms
	);
} else {
	$query_year = array(
		'taxonomy' => 'beach_access_type',
		'field' => 'slug',
		'terms' => array( date('Y') ),
		'operator' => 'IN',
		'include_children' => false // Remove if you need posts from term 7 child terms
	);
}

if(isset($_GET['discipline'])){
	$query_discipline = array(
		'taxonomy' => 'beach_access_type',
		'field' => 'slug',
		'terms' => array($_GET['discipline']),
		'operator' => 'IN',
		'include_children' => false // Remove if you need posts from term 7 child terms
	);
} else {
	$query_discipline = "";
}

$tax_query = array(
	array(
		'relation' => 'AND',
		$query_year,
		$query_discipline
	)
);

$context['events'] = Timber::get_posts(array(	'post_type' 	=> array( 'beach_access' ),
												'posts_per_page'=> -1, 
												'post_status' 	=> array( 'publish', 'future' ),
												'order'         => 'ASC',
												'orderby'       => 'date',
												'tax_query'		=> $tax_query,
												//'meta_query' 	=> $meta_query
												));

$context['season'] = $_GET['season'];
$context['discipline'] = $_GET['discipline'];

// Do not change!

$templates = array( 'archive-beach_access.twig' );
Timber::render( $templates, $context );