<?php
$context = Timber::get_context();

// Worldtours
$page = get_page_by_path( 'wingfoil-disciplines' );
$worldtours_args = new WP_Query( 
	array(
		'post_type'      => 'page',
		'posts_per_page' => 6,
		'post_parent'    => $page->ID,
		'order'          => 'ASC',
		'orderby'        => 'menu_order'
 	)
);
$context['worldtours'] = Timber::get_posts($worldtours_args);

// GWA Directory
$directorypost_args = new WP_Query( 
	array(
		'post_type'      => 'directorypost',
		'posts_per_page' => 12,
		'orderby'        => 'rand'
 	)
);
$context['directorypost'] = Timber::get_posts($directorypost_args);

// Other
$post = Timber::get_post();
$context['post'] = $post;

$context['directory'] = Timber::get_posts(array('post_type' => array( 'directorypost' )));

$context['news'] = Timber::get_posts(array('post_type' => array( 'post' ),'posts_per_page' => 6 ));

$context['events'] = Timber::get_posts(array('post_type' => array( 'events' ),'posts_per_page' => 3 ));

$video_args = new WP_Query( 
	array( 
		'posts_per_page' => -1, 
		'post_type' => array('event_report_videos','other_videos'),
		'order' => 'DESC',	
		'posts_per_page' => 3		
	)
);

$context['videos'] = Timber::get_posts($video_args);

$galleries_args = new WP_Query( 
	array( 
		'posts_per_page' => -1, 
		'post_type' => array('galleries_2018','galleries_2017'),
		'order' => 'DESC',	
		'posts_per_page' => 3		
	)
);

$context['galleries'] = Timber::get_posts($galleries_args);


$rider_context_slugs = array();

$rider_term_slugs = array('gka-kite-surf-world-tour','gka-kiteboarding-world-tour','freestyle');

foreach($rider_term_slugs as $term_slug) {
	$new_term_slug = str_replace('-', '_', $term_slug);
	$rider_context_slugs[] = $new_term_slug;
    $rider_args = array(
        'post_type' => array( 'riders' ),
        'tax_query' => array(
            array(
                'taxonomy' => 'rider_type',
                'field' => 'slug',
                'terms' => array( $term_slug),
            )
        )
    );
    $context[$new_term_slug] = Timber::get_posts($rider_args);
}
$context['rider_context_slugs'] = $rider_context_slugs;


$event_term_slugs = array('gka-kite-surf-world-tour','gka-kiteboarding-world-tour','freestyle');

foreach($event_term_slugs as $term_slug) {
	$new_term_slug = str_replace('-', '_', $term_slug);
	$new_term_slug_event = $new_term_slug . '_events';
	$event_context_slugs[] = $new_term_slug;
    $event_args = array(
        'post_type' => array( 'events' ),
        'tax_query' => array(
            array(
                'taxonomy' => 'event_type',
                'field' => 'slug',
                'terms' => array( $term_slug),
            )
        )
    );
    $context[$new_term_slug_event] = Timber::get_posts($event_args);
}


$context['riders'] = Timber::get_posts(array('post_type' => array( 'riders' )));

$context['is_front_page'] = 'true';

$templates = array( 'front-page.twig' );
Timber::render( $templates, $context );










