<?php

// --------------------------------------------
// Posts reference object (Events per discipline)
// --------------------------------------------
// '$event_reference_object' set on parent page

// --------------------------------------------
// 	Content Array: Events
// --------------------------------------------
$events = array();

// --------------------------------------------
// 	Content Array: Riders
// --------------------------------------------
$rider_names = array();
$scrubbed_rider_names = array();

// --------------------------------------------
// Content Array : Compiled event results
// --------------------------------------------
$compiled_event_results = array();

$i = 0;
$racecount = $event_reference_object->post_count;
$whilecount = $racecount -1;
$z = 0;


// --------------------------------------------
// 
//		Events
//
// --------------------------------------------
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
		// Event Title (singular)
		// 
		$event_title = get_the_title('', '', true); // set third parameter to false
		$event_visibility = get_field('show_event_table'); // set third parameter to false

		if ($event_visibility){
			$events[] = $event_title;
		}
endwhile;


// --------------------------------------------
// 
//		Riders
//
// --------------------------------------------
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
	// Get event titles
	// 
	$racetitle = $event_reference_object->post_title;
	$event_title = get_the_title('', '', true); // set third parameter to false
		
	if( have_rows('race_row') ):
		while( have_rows('race_row') ): the_row();

			// Cycle through visible events
			// --
			foreach ($events as $event ){

				// Get event properties if on the list of visible events
				// --			
				if ($event == $event_title) :


					// Get riders in each event and add them to an array 
					// (Mutiples of the same name)
					// --
					$rider_post = get_sub_field('contestant'); // post object array
					$rider_name = $rider_post->post_title;
					$rider_names[] = $rider_name;

					// Remove mutiples of the same name
					// --				
					$scrubbed_rider_names = array_unique($rider_names);

				endif;
			}	

		endwhile;
	endif;
endwhile;


// --------------------------------------------
// 
//		Placeholders for points & positions if 'NA'
//
// --------------------------------------------

// Merge races into names
// --
foreach ($scrubbed_rider_names as $rider ){
	$compiled_event_results[$rider]['rider_name'] = $rider;

	// Containers for ponts and position
	// --
	$y = 0;
	foreach ($events as $race ){
		$compiled_event_results[$rider]['races'][$race]['points'] = '-';
		$compiled_event_results[$rider]['races'][$race]['position'] = '-';
		++$y;
	}
}

// --------------------------------------------
// 
//		Push points & positions into placeholders
//
// --------------------------------------------



// Cycle through all events
// --
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
	// Get event titles
	// 
	$racetitle = $event_reference_object->post_title;
	$event_title = get_the_title('', '', true); // set third parameter to false
			

	if( have_rows('race_row') ):
		$x = 0;
		while( have_rows('race_row') ): the_row();

			// Cycle through visible events
			// --
			foreach ($events as $event ){

				// Get event properties if on the list of visible events
				// --			
				if ($event == $event_title) :

					// Names
					$rider_post = get_sub_field('contestant'); // post object array
					$rider_name = $rider_post->post_title;
					// Numbers
					$points = get_sub_field('points');
					$position = $x + 1;

					$n = 0;
					foreach ($compiled_event_results as $name ){
						// points to the names
					    $compiled_event_results[$rider_name]['races'][$event]['race_name'] = $event;
					    $compiled_event_results[$rider_name]['races'][$event]['points'] = $points;
					    $compiled_event_results[$rider_name]['races'][$event]['position'] = $position;
					}
					++$x;
				endif;
			}						
		endwhile;			
	endif;
endwhile;


// --------------------------------------------
// 
//		Re-index the contestants & races
//
// --------------------------------------------				
foreach ($scrubbed_rider_names as $rider_name ){
	$compiled_event_results['rider_' . $z] = $compiled_event_results[$rider_name];
	unset($compiled_event_results[$rider_name]);
	$d = 0;
	foreach ($events as $race ){
		$compiled_event_results['rider_' . $z]['races']['race_' . $d] = $compiled_event_results['rider_' . $z]['races'][$race];
		unset($compiled_event_results['rider_' . $z]['races'][$race]);
		++$d;					
	}
	++$z;					
}


// --------------------------------------------
// 
//		Total points for each contestant
//
// --------------------------------------------	
$c = 0;
foreach ($scrubbed_rider_names as $rider ){
	$c_races = $compiled_event_results["rider_" . $c]['races'];
	$total_points = 0;
	$rc = 0;
	foreach ($events as $race ){
		$total_points += $c_races['race_' . $rc]['points'];
		$total_points_list[$c] = $total_points;
		$compiled_event_results["rider_" . $c]['total_points'] = $total_points;
		++$rc;
	}
	++$c;
}

// --------------------------------------------
// 
//		Sort order
//
// --------------------------------------------	
$sort = array();
$g = 0;
foreach ($compiled_event_results as $key => $row)
{
    $sort[$key] = $row['total_points'];
    ++$g;
}
array_multisort($sort, SORT_DESC, $compiled_event_results);


// --------------------------------------------
// Presentation: Posts reference object (Events per discipline)
// --------------------------------------------
$context['event_reference_object'] = $event_reference_object;

// --------------------------------------------
// Presentation: Events
// --------------------------------------------
$context['events'] = $events;

// --------------------------------------------
// Admin: Riders (mutiple)
// --------------------------------------------
$context['rider_names'] = $rider_names;


// --------------------------------------------
// Presentation: Compiled event results
// --------------------------------------------
$context['compiled_event_results'] = $compiled_event_results;

?>
