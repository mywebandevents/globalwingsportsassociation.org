<?php
// ----------------------------------------
// 
// 		Events object
// 
// ----------------------------------------
$event_reference_object = new WP_Query( 
	array( 
		'posts_per_page' => -1, 
		'post_type'=> 'events',
		'order' 			=> 		'ASC',
		'tax_query' => array(
            array(
                'taxonomy' => 'event_type',
                'field' => 'slug',
                'terms' => $page_slug,
            )
        )			
	)
);
?>
