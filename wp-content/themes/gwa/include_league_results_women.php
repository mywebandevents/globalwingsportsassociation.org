<?php

// --------------------------------------------
// Posts reference object (Events per discipline)
// --------------------------------------------
// '$event_reference_object' set on parent page

// --------------------------------------------
// 	Content Array: Events
// --------------------------------------------
$events_women = array();

// --------------------------------------------
// 	Content Array: Riders
// --------------------------------------------
$rider_names_women = array();
$scrubbed_rider_names_women = array();

// --------------------------------------------
// Content Array : Compiled event results
// --------------------------------------------
$compiled_event_results_women = array();

$i = 0;
$racecount = $event_reference_object->post_count;
$whilecount = $racecount -1;
$z = 0;


// --------------------------------------------
// 
//		Events
//
// --------------------------------------------
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
		// Event Title (singular)
		// 
		$event_title = get_the_title('', '', true); // set third parameter to false
		$event_visibility = get_field('show_event_table_women'); // set third parameter to false

		if ($event_visibility == 'show'){
			$events_women[] = $event_title;
		}
endwhile;


// --------------------------------------------
// 
//		Riders
//
// --------------------------------------------
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
	// Get event titles
	// 
	$racetitle = $event_reference_object->post_title;
	$event_title_women = get_the_title('', '', true); // set third parameter to false
		
	if( have_rows('race_row_women') ):
		while( have_rows('race_row_women') ): the_row();

			// Cycle through visible events
			// --
			foreach ($events_women as $event_women ){

				// Get event properties if on the list of visible events
				// --			
				if ($event_women == $event_title_women) :


					// Get riders in each event and add them to an array 
					// (Mutiples of the same name)
					// --
					$rider_post_women = get_sub_field('contestant_woman'); // post object array
					$rider_name_women = $rider_post_women->post_title;
					$rider_names_women[] = $rider_name_women;

					// Remove mutiples of the same name
					// --				
					$scrubbed_rider_names_women = array_unique($rider_names_women);

				endif;
			}	

		endwhile;
	endif;
endwhile;


// --------------------------------------------
// 
//		Placeholders for points & positions if 'NA'
//
// --------------------------------------------

// Merge races into names
// --
foreach ($scrubbed_rider_names_women as $rider ){
	$compiled_event_results_women[$rider]['rider_name'] = $rider;

	// Containers for ponts and position
	// --
	$y = 0;
	foreach ($events_women as $race ){
		$compiled_event_results_women[$rider]['races'][$race]['points'] = '-';
		$compiled_event_results_women[$rider]['races'][$race]['position'] = '-';
		++$y;
	}
}

// --------------------------------------------
// 
//		Push points & positions into placeholders
//
// --------------------------------------------



// --------------------------------------------
// 
//		Push points & positions into placeholders
//
// --------------------------------------------



// Cycle through all events
// --
while ( $event_reference_object->have_posts()) : $event_reference_object->the_post();
		
	// Get event titles
	// 
	$racetitle = $event_reference_object->post_title;
	$event_title = get_the_title('', '', true); // set third parameter to false
			

	if( have_rows('race_row_women') ):
		$x = 0;
		while( have_rows('race_row_women') ): the_row();

			// Cycle through visible events
			// --
			foreach ($events as $event ){

				// Get event properties if on the list of visible events
				// --			
				if ($event == $event_title) :

					// Names
					$rider_post = get_sub_field('contestant_woman'); // post object array
					$rider_name = $rider_post->post_title;
					$rider_name_slug = $rider_post->post_name;
					$rider_age = $rider_post->rider_age;
					// Numbers
					$points = get_sub_field('points');
					$position = $x + 1;

					$n = 0;
					foreach ($compiled_event_results_women as $name ){
						// points to the names
					    $compiled_event_results_women[$rider_name]['rider_name_slug'] = $rider_name_slug;
					    $compiled_event_results[$rider_name]['rider_age'] = $rider_age;
					    $compiled_event_results_women[$rider_name]['races'][$event]['race_name'] = $event;
					    $compiled_event_results_women[$rider_name]['races'][$event]['points'] = $points;
					    $compiled_event_results_women[$rider_name]['races'][$event]['position'] = $position;
					}
					++$x;
				endif;
			}						
		endwhile;			
	endif;
endwhile;

// --------------------------------------------
// 
//		Re-index the contestants & races
//
// --------------------------------------------				
foreach ($scrubbed_rider_names_women as $rider_name ){
	$compiled_event_results_women['rider_' . $z] = $compiled_event_results_women[$rider_name];
	unset($compiled_event_results_women[$rider_name]);
	$d = 0;
	foreach ($events_women as $race ){
		$compiled_event_results_women['rider_' . $z]['races']['race_' . $d] = $compiled_event_results_women['rider_' . $z]['races'][$race];
		unset($compiled_event_results_women['rider_' . $z]['races'][$race]);
		++$d;					
	}
	++$z;					
}


// --------------------------------------------
// 
//		Total points for each contestant
//
// --------------------------------------------	
$c = 0;
foreach ($scrubbed_rider_names_women as $rider ){
	$c_races = $compiled_event_results_women["rider_" . $c]['races'];
	$total_points = 0;
	$rc = 0;
	foreach ($events_women as $race ){
		$total_points += $c_races['race_' . $rc]['points'];
		$total_points_list[$c] = $total_points;
		$compiled_event_results_women["rider_" . $c]['total_points'] = $total_points;
		++$rc;
	}
	++$c;
}

// --------------------------------------------
// 
//		Sort order
//
// --------------------------------------------	
$sort = array();
$g = 0;
foreach ($compiled_event_results_women as $key => $row)
{
    $sort[$key] = $row['total_points'];
    ++$g;
}
array_multisort($sort, SORT_DESC, $compiled_event_results_women);


// --------------------------------------------
// Presentation: Posts reference object (Events per discipline)
// --------------------------------------------
$context['event_reference_object'] = $event_reference_object;

// --------------------------------------------
// Presentation: Events
// --------------------------------------------
$context['events_women'] = $events_women;

// --------------------------------------------
// Admin: Riders (mutiple)
// --------------------------------------------
$context['rider_names_women'] = $rider_names_women;


// --------------------------------------------
// Presentation: Compiled event results
// --------------------------------------------
$context['compiled_event_results_women'] = $compiled_event_results_women;

?>
