<?php

// Use this file to create custom post types.
// Example post type - Change this or delete it


/* 
 * -----------------------------------------------------------------------
 *
 *        Renaming 'Posts' > 'News'
 *
 * ------------------------------------------------------------------------
 */
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );


/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: riders
 *
 * ------------------------------------------------------------------------
 */
/*
function rider_type() {
    $labels = array(
        'name'              => _x( 'Rider Type Categories', 'taxonomy general name', 't_seed_theme' ),
        'singular_name'     => _x( 'Rider Type Categories', 'taxonomy singular name', 't_seed_theme' ),
        'search_items'      => __( 'Search riders Categories', 't_seed_theme' ),
        'all_items'         => __( 'All riders Categories', 't_seed_theme' ),
        'parent_item'       => __( 'Parent riders Categories', 't_seed_theme' ),
        'parent_item_colon' => __( 'Parent riders Categories:', 't_seed_theme' ),
        'edit_item'         => __( 'Edit riders Categories', 't_seed_theme' ),
        'update_item'       => __( 'Update riders Categories', 't_seed_theme' ),
        'add_new_item'      => __( 'Add New riders Categories', 't_seed_theme' ),
        'new_item_name'     => __( 'New riders Categories', 't_seed_theme' ),
        'menu_name'         => __( 'Rider Type Categories', 't_seed_theme' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical'  => true,
        'public'        => true,
        'has_archive'   => false,
        'show_admin_column' => true,
        'rewrite'  => array( 'slug' => 'riders', 'with_front' => false ),
        // 'rewrite' => array(
        //        'slug' => '/',
        //        'with_front' => false
        //  )        
    );

    register_taxonomy( 'rider_type', 'riders', $args );
}
add_action( 'init', 'rider_type', 0 );

function riders() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Riders', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Riders', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Riders', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Riders Item:', 't_seed_theme' ),
        'all_items'             => __( 'All Riders Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Riders item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'Riders', 't_seed_theme' ),
        'description'           => __( 'Riders Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'taxonomies' => array('post_tag'),
        'rewrite'  => array( 'slug' => 'rider', 'with_front' => false ),
    );

    register_post_type( 'riders', $args );
}
add_action( 'init', 'riders', 0 );       
*/ 



/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Wingfoil Schools
 *
 * ------------------------------------------------------------------------
 */

function wingfoil_schools_type() {
    $labels = array(
        'name'              => _x( 'Wingfoil Schools Categories', 'taxonomy general name', 't_seed_theme' ),
        'singular_name'     => _x( 'Wingfoil Schools Categories', 'taxonomy singular name', 't_seed_theme' ),
        'search_items'      => __( 'Search Wingfoil Schools Categories', 't_seed_theme' ),
        'all_items'         => __( 'All Wingfoil Schools Categories', 't_seed_theme' ),
        'parent_item'       => __( 'Parent Wingfoil Schools Categories', 't_seed_theme' ),
        'parent_item_colon' => __( 'Parent Wingfoil Schools Categories:', 't_seed_theme' ),
        'edit_item'         => __( 'Edit Wingfoil Schools Categories', 't_seed_theme' ),
        'update_item'       => __( 'Update Wingfoil Schools Categories', 't_seed_theme' ),
        'add_new_item'      => __( 'Add New Wingfoil Schools Categories', 't_seed_theme' ),
        'new_item_name'     => __( 'New Wingfoil Schools Categories', 't_seed_theme' ),
        'menu_name'         => __( 'Wingfoil Schools Categories', 't_seed_theme' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical'  => true,
        'public'        => true,
        'has_archive'   => true,
        'show_admin_column' => true,
        'rewrite'       => array( 'slug' => 'wingfoil-schools-type' ),
    );

    register_taxonomy( 'wingfoil-schools-type', 'wingfoil-schools', $args );
}
add_action( 'init', 'wingfoil_schools', 0 );


function wingfoil_schools() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Wingfoil Schools', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Wingfoil School', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Wingfoil Schools', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Wingfoil School:', 't_seed_theme' ),
        'all_items'             => __( 'All Wingfoil Schools', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Wingfoil School', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'Wingfoil Schools', 't_seed_theme' ),
        'description'           => __( 'Wingfoil Schools Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'taxonomies' => array('post_tag'),
        'rewrite'  => array( 'slug' => 'wingfoil-schools', 'with_front' => true ),
    );

    register_post_type( 'wingfoil-schools', $args );
}
add_action( 'init', 'wingfoil_schools', 0 );


/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: GWA Race Class Members
 *
 * ------------------------------------------------------------------------
 */

function race_class_members_type() {
    $labels = array(
        'name'              => _x( 'GWA Race Class Members Categories', 'taxonomy general name', 't_seed_theme' ),
        'singular_name'     => _x( 'GWA Race Class Members Categories', 'taxonomy singular name', 't_seed_theme' ),
        'search_items'      => __( 'Search GWA Race Class Members Categories', 't_seed_theme' ),
        'all_items'         => __( 'All GWA Race Class Members Categories', 't_seed_theme' ),
        'parent_item'       => __( 'Parent GWA Race Class Members Categories', 't_seed_theme' ),
        'parent_item_colon' => __( 'Parent GWA Race Class Members Categories:', 't_seed_theme' ),
        'edit_item'         => __( 'Edit GWA Race Class Members Categories', 't_seed_theme' ),
        'update_item'       => __( 'Update GWA Race Class Members Categories', 't_seed_theme' ),
        'add_new_item'      => __( 'Add New GWA Race Class Members Categories', 't_seed_theme' ),
        'new_item_name'     => __( 'New GWA Race Class Members Categories', 't_seed_theme' ),
        'menu_name'         => __( 'GWA Race Class Members Categories', 't_seed_theme' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical'  => true,
        'public'        => true,
        'has_archive'   => true,
        'show_admin_column' => true,
        'rewrite'       => array( 'slug' => 'race-class-members-type' ),
    );

    register_taxonomy( 'race-class-members-type', 'wingfoil-schools', $args );
}
add_action( 'init', 'race_class_members', 0 );


function race_class_members() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'GWA Race Class Members', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'GWA Race Class Member', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'GWA Race Class Members', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent GWA Race Class Members:', 't_seed_theme' ),
        'all_items'             => __( 'All GWA Race Class Members', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New GWA Race Class Members', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'GWA Race Class Members', 't_seed_theme' ),
        'description'           => __( 'GWA Race Class Members Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'taxonomies' => array('post_tag'),
        'rewrite'  => array( 'slug' => 'race-class-members', 'with_front' => true ),
    );

    register_post_type( 'race-class-members', $args );
}
add_action( 'init', 'race_class_members', 0 );

/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Tasks
 *
 * ------------------------------------------------------------------------
 */

/*
function gka_tasks() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'GKA Tasks', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'GKA Tasks', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Tasks', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent tasks Item:', 't_seed_theme' ),
        'all_items'             => __( 'All tasks Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New tasks item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'Tasks', 't_seed_theme' ),
        'description'           => __( 'Tasks Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'taxonomies' => array('post_tag'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'gka-tasks', 'with_front' => false ),
    );

    register_post_type( 'gka_tasks', $args );
}
add_action( 'init', 'gka_tasks', 0 );
*/


/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: GWA Members
 *
 * ------------------------------------------------------------------------
 */

function directory_taxonomy() {
    $labels = array(
        'name'              => _x( 'GWA Members Type', 'taxonomy general name', 't_seed_theme' ),
        'singular_name'     => _x( 'GWA Members Type', 'taxonomy singular name', 't_seed_theme' ),
        'search_items'      => __( 'Search task Type', 't_seed_theme' ),
        'all_items'         => __( 'All task Type', 't_seed_theme' ),
        'parent_item'       => __( 'Parent task Type', 't_seed_theme' ),
        'parent_item_colon' => __( 'Parent task Type:', 't_seed_theme' ),
        'edit_item'         => __( 'Edit task Type', 't_seed_theme' ),
        'update_item'       => __( 'Update task Type', 't_seed_theme' ),
        'add_new_item'      => __( 'Add New task Type', 't_seed_theme' ),
        'new_item_name'     => __( 'New task Type', 't_seed_theme' ),
        'menu_name'         => __( 'GWA Members Type', 't_seed_theme' ),
    );

    $args = array(
        'labels' => $labels,
        'hierarchical'  => true,
        'public'        => true,
        'has_archive'   => true,
        'show_admin_column' => true,
        'rewrite'  => array( 'slug' => 'directory', 'with_front' => false ),
    );
    register_taxonomy( 'task-type', 'directorypost', $args );
}
add_action( 'init', 'directory_taxonomy', 0 );

function directorypost() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'GWA Members', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'GWA Members', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'GWA Members', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent GWA Members Item:', 't_seed_theme' ),
        'all_items'             => __( 'All GWA Members Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New GWA Members item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'GWA Members', 't_seed_theme' ),
        'description'           => __( 'GWA Members Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'taxonomies' => array('post_tag'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'gka-directory', 'with_front' => false ),
    );

    register_post_type( 'directorypost', $args );
}
add_action( 'init', 'directorypost', 0 );





/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Event videos
 *
 * ------------------------------------------------------------------------
 */

/*
function event_report_videos() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Event Report Videos', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Event Report Videos', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Event Report Videos', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Event Report Videos Item:', 't_seed_theme' ),
        'all_items'             => __( 'All Event Report Videos Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Event Report Videos item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set Event Report Videos options for Custom Post Type
    $args = array(
        'label'                 => __( 'Event Report Videos', 't_seed_theme' ),
        'description'           => __( 'Event Report Videos Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'event-report-videos', 'with_front' => false ),
        'taxonomies' => array('post_tag'),
    );

    register_post_type( 'event_report_videos', $args );
}
add_action( 'init', 'event_report_videos', 0 );
*/

/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Event Report Videos
 *
 * ------------------------------------------------------------------------
 */

/*
function other_videos() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Other Videos', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Other Videos', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Other Videos', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Other Videos Item:', 't_seed_theme' ),
        'all_items'             => __( 'All Other Videos Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Other Videos item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other Videos options for Custom Post Type
    $args = array(
        'label'                 => __( 'Other Videos', 't_seed_theme' ),
        'description'           => __( 'Other Videos Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'taxonomies' => array('post_tag'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'other-videos', 'with_front' => false ),
        'taxonomies' => array('post_tag'),
    );

    register_post_type( 'other_videos', $args );
}
add_action( 'init', 'other_videos', 0 );
*/

/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Galleries 2018
 *
 * ------------------------------------------------------------------------
 */

/*
function galleries_2018() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Galleries 2018', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Galleries 2018', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Galleries 2018', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Galleries 2018 Item:', 't_seed_theme' ),
        'all_items'             => __( 'All Galleries 2018 Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Galleries 2018 item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'Galleries 2018', 't_seed_theme' ),
        'description'           => __( 'Galleries 2018 Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'galleries-2018', 'with_front' => false ),
        'taxonomies' => array('post_tag'),
    );

    register_post_type( 'galleries_2018', $args );
}
add_action( 'init', 'galleries_2018', 0 );
*/

/* 
 * -----------------------------------------------------------------------
 *
 *        Custom Post Type: Galleries 2017
 *
 * ------------------------------------------------------------------------
 */

/*
function galleries_2017() {
    // Set UI labels for Custom Post Type
    $labels = array(
        'name'                  => _x( 'Galleries 2017', 'Post Type General Name', 't_seed_theme' ),
        'singular_name'         => _x( 'Galleries 2017', 'Post Type Singular Name', 't_seed_theme' ),
        'name_admin_bar'        => __( 'Galleries 2017', 't_seed_theme' ),
        'parent_item_colon'     => __( 'Parent Galleries 2017 Item:', 't_seed_theme' ),
        'all_items'             => __( 'All Galleries 2017 Items', 't_seed_theme' ),
        'add_new_item'          => __( 'Add New Galleries 2017 item', 't_seed_theme' ),
        'add_new'               => __( 'Add New', 't_seed_theme' ),
        'new_item'              => __( 'New Item', 't_seed_theme' ),
        'edit_item'             => __( 'Edit Item', 't_seed_theme' ),
        'update_item'           => __( 'Update Item', 't_seed_theme' ),
        'view_item'             => __( 'View Item', 't_seed_theme' ),
        'search_items'          => __( 'Search Item', 't_seed_theme' ),
        'not_found'             => __( 'Not found', 't_seed_theme' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 't_seed_theme' ),
        'items_list'            => __( 'Items list', 't_seed_theme' ),
        'items_list_navigation' => __( 'Items list navigation', 't_seed_theme' ),
        'filter_items_list'     => __( 'Filter items list', 't_seed_theme' ),
        'attributes'            => __( 'Page Attributes', 't_seed_theme' ),
    );
    // Set other options for Custom Post Type
    $args = array(
        'label'                 => __( 'Galleries 2017', 't_seed_theme' ),
        'description'           => __( 'Galleries 2017 Custom Post', 't_seed_theme' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'revisions', 'thumbnail', 'excerpt', 'page-attributes', 'taxonomies'),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'menu_icon'             => 'dashicons-media-text',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'  => array( 'slug' => 'galleries-2017', 'with_front' => false ),
        'taxonomies' => array('post_tag'),
    );

    register_post_type( 'galleries_2017', $args );
}
add_action( 'init', 'galleries_2017', 0 );

*/
