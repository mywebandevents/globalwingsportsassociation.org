<?php


// Googlemap API key

// https://www.aliciaramirez.com/2015/02/advanced-custom-fields-google-maps-tutorial/

 // AIzaSyALtsZlr4Hvk57OylPV2jZ21w6wGkEQUUc

function my_theme_add_scripts() {
	// wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyALtsZlr4Hvk57OylPV2jZ21w6wGkEQUUc', array(), '3', true );
	// wp_enqueue_script( 'google-map-init', get_template_directory_uri() . '/library/js/google-maps.js', array('google-map', 'jquery'), '0.1', true );
}

add_action( 'wp_enqueue_scripts', 'my_theme_add_scripts' );

function my_acf_google_map_api( $api ){
	
	$api['key'] = 'AIzaSyALtsZlr4Hvk57OylPV2jZ21w6wGkEQUUc';
	
	return $api;
	
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
