<?php


//==============================================================================
// ENQUEUE SCRIPTS AND STYLES FOR THEME OPTIONS
//==============================================================================


if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'     => 'Website General Settings',
        'menu_title'    => 'Site Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
}

