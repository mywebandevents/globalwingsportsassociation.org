<?php

//==============================================================================
// SCRIPTS & ENQUEUEING
//==============================================================================

add_action( 'wp_enqueue_scripts', 't_seed_scripts_and_styles', 999 );

function t_seed_scripts_and_styles() {
	global $wp_styles;
	if (!is_admin()) {

		// Load asset manifest
		$assetstr = file_get_contents(dirname(dirname(__FILE__))."/build/manifest.json");
		$assets = json_decode($assetstr, true);
		
		// Load font awesome from visual composer
		wp_enqueue_style( 'font-awesome' );
		
		// Load css and js		
		wp_register_style( 'old-stylesheet', get_stylesheet_directory_uri() . '/build/css/styles.min.css?v=1.0.0', array(), '', 'all' );
		wp_enqueue_style( 'old-stylesheet' );
		wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/src/css/style.css?v=1.0.1', array(), '', 'all' ); 
		wp_enqueue_style( 'stylesheet' );

		// Load jQuery
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://code.jquery.com/jquery-2.2.4.min.js', false, '2.2.4');

		//wp_register_style( 'seed-stylesheet', get_stylesheet_directory_uri() . $assets['css'], array(), '', 'all' );
		//wp_enqueue_style( 'seed-stylesheet' );
		
		//wp_register_script( 'main', get_stylesheet_directory_uri().'/src/js/main.js', false, '1.0');
		//wp_register_script( 'scripts', get_stylesheet_directory_uri() . $assets['js'], array(), '', true ); 
		//wp_register_script( 'lazyload', 'https://cdnjs.cloudflare.com/ajax/libs/jquery_lazyload/1.9.7/jquery.lazyload.js', false, '1.9.7');	

		// Do it.
		wp_enqueue_script( 'jquery' );
		//wp_enqueue_script( 'main' );
		//wp_enqueue_script( 'scripts' );
		//wp_enqueue_script( 'lazyload' );
		//wp_enqueue_script( 'modernizr' );


	}
}



