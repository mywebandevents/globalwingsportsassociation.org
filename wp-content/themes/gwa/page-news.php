<?php
/*
Template Name: News page template
*/
$context = Timber::get_context();
// https://bigbitecreative.com/digging-into-timber/
$context['cat_name'] = 'news';
$context['cat_id'] = get_cat_ID( $context['cat_name'] );
function get_cat_slug ($category_id) {
	$category = & get_category ((int) $category_id);
	return $category-> slug;
}
$context['cat_slug'] = get_cat_slug($context['cat_id']);
$context['categories'] = Timber::get_terms( 'category', array('hide_empty' => 1,'exclude' => '1,11'));

$cat_args = array(
	'posts_per_page' => -1,
	'tax_query' => array(
        array(
            'taxonomy' => 'category',   // taxonomy name
            'field' => 'slug',
            'terms' => $context['cat_slug'], // term id, term slug or term name
        )
    )				
);

$context['posts'] = Timber::get_posts($cat_args);

$templates = array( 'archive.twig' );
Timber::render( $templates, $context );
