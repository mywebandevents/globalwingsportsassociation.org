<?php
/*
Template Name: Page Wingfoil Race Class
*/

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

// Directory
$context['directory'] = Timber::get_posts(array('post_type' => array( 'directorypost' )));

$post = Timber::get_post();

$context['post'] = $post;

$post_slug = $post->slug;

$context['post_slug'] = $post_slug;


$url = $_SERVER['REQUEST_URI'];
$substr_url = substr($url, strrpos($url, 's-') + 2);
$last_word = rtrim($substr_url, '/');



$context['last_word'] = $last_word;

$eventterm = array(); 

$terms = get_terms( array(
	'taxonomy' => 'event_type',
	'hide_empty' => false,
) );

// $context['terms'] = $terms;

// foreach ($terms as $term ){
// 	$eventterm[] = $term->slug;
// }

$page_slug = $post->slug;
$context['post_slug'] = $page_slug;

$templates = array(
    'page-wingfoil-race-class.twig' 
);
$context['sidebar'] = Timber::get_sidebar('sidebar.php');
Timber::render( $templates, $context );

