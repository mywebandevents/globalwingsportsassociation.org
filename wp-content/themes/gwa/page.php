<?php
/*
Template Name: Page template
*/

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

// Directory
$context['directory'] = Timber::get_posts(array('post_type' => array( 'directorypost' )));
$context['race_class_members'] = Timber::get_posts(array('post_type' => array( 'race-class-members' )));

$post = Timber::get_post();

$context['post'] = $post;


$post_slug = $post->slug;

$context['post_slug'] = $post_slug;


$url = $_SERVER['REQUEST_URI'];
$substr_url = substr($url, strrpos($url, 's-') + 2);
$last_word = rtrim($substr_url, '/');



$context['last_word'] = $last_word;

$eventterm = array(); 

$terms = get_terms( array(
	'taxonomy' => 'event_type',
	'hide_empty' => false,
) );

// $context['terms'] = $terms;

// foreach ($terms as $term ){
// 	$eventterm[] = $term->slug;
// }

$page_slug = $post->slug;
$context['post_slug'] = $page_slug;

include('include_events_object.php');
include('include_league_results.php');
include('include_league_results_women.php');

$templates = array(
    'page-' . $post->post_name . '.twig',
    'page.twig'
);
$context['sidebar'] = Timber::get_sidebar('sidebar.php');
Timber::render( $templates, $context );

