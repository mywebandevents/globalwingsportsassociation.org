<?php
/*
Template Name: Page template
*/
$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

$post = Timber::get_post();

$context['post'] = $post;


$post_slug = $post->slug;

$context['post_slug'] = $post_slug;


$url = $_SERVER['REQUEST_URI'];
$substr_url = substr($url, strrpos($url, 's-') + 2);
$last_word = rtrim($substr_url, '/');



$context['last_word'] = $last_word;
$context['test'] = 'testes';

if ( function_exists( 'yoast_breadcrumb' ) ) {
    $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">','</nav>', false );
}

include('include_events_object.php');
include('include_league_results.php');

$templates = array( 'riders-page.twig' );
$context['sidebar'] = Timber::get_sidebar('sidebar.php');
Timber::render( $templates, $context );

