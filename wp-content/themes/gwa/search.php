
<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package 	WordPress
 * @subpackage 	Timber
 * @since 		Timber 0.1
 */


	$templates = array('archive.twig');
	$context = Timber::get_context();
	$context['title'] = 'Search results for "<b>'. get_search_query() . '</b>"';
	

	$context['searchresults'] = SearchContent(get_search_query());



	$context['posts'] = Timber::get_posts();
	Timber::render($templates, $context);
