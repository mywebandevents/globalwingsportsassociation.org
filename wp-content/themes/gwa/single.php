<?php
/*
Template Name: Single 'post' template
*/

// --------------------------------------------------------------------------
// 
// 		related posts
//		https://code.tutsplus.com/tutorials/creating-a-list-of-posts-with-the-same-categories-as-the-current-one--cms-22626
// 
// --------------------------------------------------------------------------

$context = Timber::get_context();

$post = Timber::get_post();
$context['post'] = $post;

// Post terms
// --
if ( function_exists( 'yoast_breadcrumb' ) ) {
    $context['breadcrumbs'] = yoast_breadcrumb('<nav id="breadcrumbs" class="main-breadcrumbs">','</nav>', false );
}

// Post terms
// --
$postTerms = $post->terms;
foreach ($postTerms as $term ){
    $postTermsSlug = $term->slug;
    $postTermsName = $term->name;
}

$context['postTerms'] = $postTerms;
// Post Taxonomy
// --
$taxonomies = get_object_taxonomies($post_type);
foreach ($taxonomies as $taxonomy ){
    $post_taxonomy = $taxonomy;
}
$context['post_taxonomy'] = $post_taxonomy;

// Query
// --
$postType = $post->type; // eventspost | riderspost
$currentID = $post->ID;
if($postType == 'riders' ) {
    $related_posts_args = array(
        'post_type' => $postType,
        'post__not_in' => array($currentID),
        'tax_query' => array(
            array(
                'taxonomy' => $post_taxonomy,
                'field'    => 'slug',
                'terms'    => $postTermsSlug,
            ),
        ),
    );
} else {
    $related_posts_args = array(
        'post_type' => $postType,
        'post__not_in' => array($currentID),      
    );    
}
$context['postType'] = $postType;
$context['related_posts'] = Timber::get_posts($related_posts_args);
$context['postTermsSlug'] = $postTermsSlug;
$context['postTermsName'] = $postTermsName;

include('include_events_object.php');
include('include_league_results.php');

$context['directory'] = Timber::get_posts(array('post_type' => array( 'directorypost' )));
$context['sidebar'] = Timber::get_sidebar('include-sidebar.php');
$templates = array( 'single-'.$postType.'.twig', 'single.twig' );
Timber::render( $templates, $context );
