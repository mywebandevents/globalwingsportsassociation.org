// handle links with @href started with '#' only
$(document).on('click', 'a[href^="#"]', function(e) {
    // target element id
    var id = $(this).attr('href');

    // target element
    var $id = $(id);
    if ($id.length === 0) {
        return;
    }

    // prevent standard hash navigation (avoid blinking in IE)
    e.preventDefault();

    // top position relative to the document
    var pos = $id.offset().top - 120;

    // animated top scrolling
    $('body, html').animate({scrollTop: pos});
});

$(document).ready(function() {
	$("#association").on("click", function(){
		$("#split-page-layover").fadeOut();
	});
	
	$("#worldtour").on("click", function(){
		window.location.href = "https://www.gkakiteworldtour.com/";
	});
});

// Add button on homepage
//$('.slides').each(function() {
	if($(".slider-button").length > 0) {
		//console.log("Have slider-button"); 
	} else {
		var link = $('.caption').parent().prev().attr('href');
  		$('.caption').append("<div class='slider-button'><a href='"+link+"'>READ MORE</div>");
		//console.log("No slider-button");
	}
//}); 

$("#changeCountry").on("change", function () {
    var country = $("#changeCountry").find('option:selected').attr("value");
    //alert(year);

    var queryString = location.search;
	var uri = '/wingfoil-schools/'+queryString;
    var key = 'country';

    window.location.href = updateQueryStringParameter(uri, key, country);
});

$("#changeType").on("change", function () {
    var type = $("#changeType").find('option:selected').attr("name");
    //alert(year);

    var queryString = location.search;
	var uri = '/beach-access/'+queryString;
    var key = 'type';

    window.location.href = updateQueryStringParameter(uri, key, type);
});

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}