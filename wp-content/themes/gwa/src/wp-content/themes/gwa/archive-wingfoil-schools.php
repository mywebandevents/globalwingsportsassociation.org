<?php
/*
Template Name: Wingfoil Schools
*/

$context = Timber::get_context();
// $context['posts'] = Timber::get_posts();

$post = Timber::get_post();

$context['post'] = $post;

$post_slug = $post->slug;

$context['post_slug'] = $post_slug;

$context['post_title'] = $post->title;

$context['post_excerpt'] = get_the_excerpt($post);


$url = $_SERVER['REQUEST_URI'];
$substr_url = substr($url, strrpos($url, 's-') + 2);
$last_word = rtrim($substr_url, '/');

$context['last_word'] = $last_word;

$eventterm = array(); 

$terms = get_terms( array(
	'taxonomy' => 'wingfoil-schools-type',
	'hide_empty' => false,
) );

// $context['terms'] = $terms;

// foreach ($terms as $term ){
// 	$eventterm[] = $term->slug;
// }

$page_slug = $post->slug;
$context['post_slug'] = $page_slug;

// Meta

if(isset($_GET['country'])){
	
	$query_country = array(
		'key'       => 'country',
        'value'     => $_GET['country'],
        'compare'   => 'LIKE'
	);
}

$meta_query = array(
	array(
		'relation' => 'AND',
		$query_country/*,
		$query_type*/
	)
);

// Tax

if(isset($_GET['type'])){
	
	$query_type = array(
		'taxonomy' => 'wingfoil-schools-type',
		'field' => 'slug',
		'terms' => array($_GET['type']),
		'operator' => 'IN',
		'include_children' => false // Remove if you need posts from term 7 child terms
	);
}

$tax_query = array(
	array(
		'relation' => 'AND',
		$query_type/*,
		$query_type*/
	)
);

$context['wingfoil_schools'] = Timber::get_posts(array(	'post_type' 	=> array( 'wingfoil-schools' ),
														'posts_per_page'=> -1, 
														'post_status' 	=> array( 'publish', 'future' ),
														'order'         => 'ASC',
														'orderby'       => 'date',
														'tax_query'		=> $tax_query,
														'meta_query' 	=> $meta_query
													));

$context['season'] = $_GET['season'];
$context['discipline'] = $_GET['discipline'];

// Do not change!

$templates = array( 'archive-wingfoil-schools.twig' );
Timber::render( $templates, $context );