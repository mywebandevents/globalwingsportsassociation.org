<?php
/*
Template Name: Archive template
*/
$context = Timber::get_context();

$context['posts'] = Timber::get_posts();

$context['term'] = new Timber\Term();

$context['get_post_type_object'] = get_post_type_object( $post_type );

$context['get_post_type_labels'] = get_post_type_labels( $post_type );

$context['get_taxonomy_labels'] = get_taxonomy_labels( $post_type );

$get_queried_object = get_queried_object();

$context['get_queried_object'] = $get_queried_object;


$get_queried_object_post_type_name = $get_queried_object->name;
$new_get_queried_object_post_type_name = str_replace('-', '_', $get_queried_object_post_type_name);


$context['get_taxonomy_labels'] = get_taxonomy_labels( $post_type );

$context['hero_image'] = get_field('' . $new_get_queried_object_post_type_name . '_hero_image', 'options');
$context['hero_image_caption'] = get_field('' . $new_get_queried_object_post_type_name . '_hero_image_caption', 'options');

$context['title'] = post_type_archive_title( '', false );

$templates = array( 'archive.twig' );
Timber::render( $templates, $context  );


