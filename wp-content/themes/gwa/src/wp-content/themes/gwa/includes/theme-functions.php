<?php

add_filter( 'novo_map_allowed_post_type', 'novo_map_post_types' );
function novo_map_post_types($types) {
    $types = array( 'post', 'page', 'wingfoil-schools' );
    return $types;
}

// Changes tiled gallery width

function custom_tiled_gallery_width( $custom_hello_text ) {
	return 844;
}

add_filter( 'tiled_gallery_content_width','custom_tiled_gallery_width');


function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


function add_custom_types_to_tax( $query ) {
    if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

        // Get all your post types
        $post_types = get_post_types();

        $query->set( 'post_type', $post_types );
        return $query;
    }
}


// flush_rewrite_rules( $hard );
register_deactivation_hook( __FILE__, 'flush_rewrite_rules' );
register_activation_hook( __FILE__, 'wdocs_flush_rewrites' );
 
 
/**
 * Flush rewrite rules on activation
 */
function wpdocs_flush_rewrites() {
    // call your CPT registration function here (it should also be hooked into 'init')
    wpdocs_custom_post_types_registration();
    flush_rewrite_rules();
}

// disable for posts
add_filter('use_block_editor_for_post', '__return_false', 10);

// disable for post types
add_filter('use_block_editor_for_post_type', '__return_false', 10);

// Write log
if ( ! function_exists( 'write_log' ) ) {

    function write_log( $log ) {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }

}



// Write log
if ( ! function_exists( 'write_log' ) ) {

    function write_log( $log ) {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }

}

// Write log
if ( ! function_exists( 'write_log' ) ) {

    function write_log( $log ) {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }

}
