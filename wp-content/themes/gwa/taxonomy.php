<?php
/*
Template Name: Taxonomy template
*/
$context = Timber::get_context();
$posts = Timber::get_posts();
$context['posts'] = Timber::get_posts();

// $term = Timber::get_terms();

$term = new Timber\Term();

$term_slug = $term->slug; // wave, freestyle
$term_tax = $term->taxonomy; // events, 

$term_slug = $term->slug;
$new_term_slug = str_replace('-', '_', $term_slug);
$context['term_slug'] = $term_slug;
$context['new_term_slug'] = $new_term_slug;

foreach ($posts as $post ){
    $post_slug = $post->type->slug;
    // $post_slug = str_replace('-', '_', $post_slug);
}
$context['post_slug'] = $post_slug;

if($term_slug){
	$context['hero_image'] = get_field('' . $new_term_slug . '_' . $post_slug . '_hero_image', 'options');
	$context['hero_image_caption'] = get_field('' . $new_term_slug . '_' . $post_slug . '_hero_image_caption', 'options');	
} else {
	$context['test'] = get_field('' . $term_tax . '_hero_image', 'options');
}


$context['term'] = new Timber\Term(); 
$templates = array( 'taxonomy.twig' );
Timber::render( $templates, $context  );


