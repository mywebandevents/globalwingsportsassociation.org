<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pfeiffer_staging');

/** MySQL database username */
define('DB_USER', 'pfeiffer_sta77');

/** MySQL database password */
define('DB_PASSWORD', 'kVee4&82sVnf5^64');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+B*D)9,~Y[z0j~G0Nw~*?Y|A%8i&7^!4Mu7C1<|q[bj($0u+y`s|%|]loLwoEU86');
define('SECURE_AUTH_KEY',  'q/!]js4k@iP<Gv<,hg<vMfnd+V0}D)*ARk*qgV|Q&@QGe<6~n<d+EUCUyDT[-7;>');
define('LOGGED_IN_KEY',    '*;L#q(,S?OkeN#|43j>Jbf@Z.P(xJiMj3JTGcR}ZMzn|Z-mGh|e%;;e=&:2&Ik;*');
define('NONCE_KEY',        'a8U,t?-Fm?c?)|ae][NlomOE5{Iq?+0! lDmm[>DroSg)<v9Y}|!2^g.82N@|z+N');
define('AUTH_SALT',        '*VJ|,Bb=FFK#LI=}RY<9g+A]IZk&!dD4pkJP;vf}|mM n|L&|5|PtT*,O .^p8_g');
define('SECURE_AUTH_SALT', 'm|;RdMV&QF|q9Pl5m|W%!%y(TD4tH`5b?$pLS4A!ojmQ}]Lw1`5v!Wp_F#FA>V>a');
define('LOGGED_IN_SALT',   '~<&q{|rt-%nxfWpt8#]}^b#TcC%{E,axyw>.Np5&v{[6W`K=?^I+F|dXqd,bP/pX');
define('NONCE_SALT',       'fD,k55>]{L`39VY))[_Jt,+sdT07.hnwtRXVc3&dnG.L6^B1G:ZNXYVk;6:39}l8');

define( 'WPCF7_AUTOP', false );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

 define ('WP_POST_REVISIONS', 3);
// define(‘WP_POST_REVISIONS’, true);

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

# Disables all core updates. Added by SiteGround Autoupdate:
define( 'WP_AUTO_UPDATE_CORE', false );
